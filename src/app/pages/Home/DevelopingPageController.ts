const viewDeveloping = require("./DevelopingPage.html");

export class DevelopingPageController {
    viewDeveloping = viewDeveloping;
    viewDevelopingId = "pageDeveloping"
    getView(): [string, DocumentFragment] {
        return [this.viewDevelopingId, document.createRange().createContextualFragment(viewDeveloping)];
    }
}
