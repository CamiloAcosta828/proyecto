const viewAboutMe = require("./AboutMe.html");

export class AboutMeController{
    viewAboutMe = viewAboutMe;
    viewAboutMeId = "pageAboutMe"

    getView(): [string, DocumentFragment] {
        return [this.viewAboutMeId, document.createRange().createContextualFragment(viewAboutMe)];
    }
}
