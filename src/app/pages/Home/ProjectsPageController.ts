const viewProject = require("./ProjectsPage.html");

export class ProjectPageController {
    viewProject = viewProject;
    viewProjectId = "pageProject"
    getView(): [string, DocumentFragment] {
        return [this.viewProjectId, document.createRange().createContextualFragment(viewProject)];
    }
}
