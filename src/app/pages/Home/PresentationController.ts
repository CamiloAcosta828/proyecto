const presentationView = require("./Presentation.html");

export class PresentationController {
  viewPresentation = presentationView;
  viewPresentationId = "presentation";

  getView(): [string, DocumentFragment] {
    return [
      this.viewPresentation,
      document.createRange().createContextualFragment(presentationView),
    ];
  }
}
