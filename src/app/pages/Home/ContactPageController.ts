const viewContact = require("./ContactPage.html");

export class ContactPageController {
    viewContact = viewContact;
    viewContactId = "pageContact"
    getView(): [string, DocumentFragment] {
        return [this.viewContactId, document.createRange().createContextualFragment(viewContact)];
    }
}
