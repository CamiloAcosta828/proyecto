const vista = require("./ChargePage.html");

export class ChargePageController{
    vista = vista;
    vistaId = "PantallaCarga";

    getView(): [string, DocumentFragment] {
        return [this.vistaId, document.createRange().createContextualFragment(vista)];
    }
}