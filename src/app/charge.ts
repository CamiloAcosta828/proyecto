export function chargeP(){
    console.log("Exporto bien");
}

/**
 * Funcion que me permite hacer la carga de la pagina principal 
 * lo que hace es que en el la ventanda le aplicamos la funcion onload
 * luego con getElementById escogemos el elemento con el id PantallaCarga
 * y cuando se cumple en onload se le cambia el estilo al elemento seleccionado
 * @function cargar
 */
export function cargar(){
    window.onload = function(){
        var carga = document.getElementById("PantallaCarga");

        carga.style.visibility = "hidden";
        carga.style.transition = "0.8s ease"
        carga.style.opacity = "0";
    }
    //document.getElementById("PantallaCarga").style.display = "none"
}